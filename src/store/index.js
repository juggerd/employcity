import { createStore } from "vuex";
import goods from "./modules/goods";

export default new createStore({
    modules: {
        goods,
    }
});