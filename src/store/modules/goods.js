import data_stub from '../../stubs/data.json'
import names_stub from '../../stubs/names.json'
import random from '@/utils/random';

export default {
    actions: {
        async fetchUSD({ commit }) {
            let cost = random(20, 80);
            console.log('USD:', cost);
            commit('updateUSD', cost);
            return cost;
        },
        async fetchRawData({ dispatch, commit }) {
            await dispatch('fetchUSD');
            commit('updateRawData', data_stub.Value.Goods);
            return data_stub.Value.Goods;
        },
        async fetchGoods({ dispatch, commit }) {
            let raw_data = await dispatch('fetchRawData');
            let params = {};
            for (let val of raw_data) {
                let key = val.G + '_' + val.T;
                params[key] = {
                    cost: parseFloat(val.C),
                    quantity: parseInt(val.P)
                };
            }
            let goods = [];
            for (let group_id in names_stub) {
                let group_obj = names_stub[group_id];
                for (let id in group_obj.B) {
                    let cost = 0;
                    let quantity = 0;
                    let key = group_id + '_' + id;
                    if (key in params) {
                        cost = params[key].cost
                        quantity = params[key].quantity;
                    }
                    goods.push({
                        'id': parseInt(id),
                        'name': group_obj.B[id].N,
                        'group_id': parseInt(group_id),
                        'group_name': group_obj.G,
                        'cost': cost,
                        'quantity': quantity
                    });
                }
            }

            commit('updateGoods', goods);
        }
    },
    mutations: {
        updateUSD(state, usd) {
            state.usd = usd;
        },
        updateRawData(state, raw_data) {
            state.raw_data = raw_data;
        },
        updateGoods(state, goods) {
            state.goods = goods;
        },
        addToCart(state, good) {
            let shop_good = state.goods.find(item => {
                return item.group_id == good.group_id && item.id == good.id;
            });
            if (!shop_good) {
                return;
            }
            --shop_good.quantity;
            let card_good = state.cart.find(item => {
                return item.group_id == good.group_id && item.id == good.id;
            });
            if (!card_good) {
                let clone = Object.assign({}, shop_good);
                clone.quantity = 1;
                state.cart.push(clone);
            } else {
                ++card_good.quantity;
            }
        },
        deleteFromCart(state, good) {
            let cart_good = state.cart.find(item => {
                return item.group_id == good.group_id && item.id == good.id && item.quantity - 1 >= 0;
            });
            if (!cart_good) {
                return;
            }
            --cart_good.quantity;
            let shop_good = state.goods.find(item => {
                return item.group_id == good.group_id && item.id == good.id;
            });
            ++shop_good.quantity;
        },
    },
    state: {
        usd: 0,
        raw: [],
        goods: [],
        cart: [],
    },
    getters: {
        getUSD(state) {
            return state.usd;
        },
        getGroupedGoods(state) {
            let goods = {};
            for (let good of state.goods) {
                if (!(good.group_id in goods)) {
                    goods[good.group_id] = {
                        'id': good.group_id,
                        'name': good.group_name,
                        'items': []
                    };
                }
                let clone = Object.assign({}, good);
                clone.cost = parseFloat((good.cost * state.usd).toFixed(2));
                goods[good.group_id].items.push(clone);
            }
            return Object.values(goods);
        },
        getCartGoods(state) {
            let goods = [];
            for (let good of state.cart) {
                let clone = Object.assign({}, good);
                clone.cost = parseFloat((good.cost * state.usd).toFixed(2));
                goods.push(clone);
            }
            return goods;
        },
        getCartGoodsTotal(state) {
            let total = state.cart.reduce((acc, val) => {
                return acc + (val.cost * val.quantity * state.usd);
            }, 0)
            return parseFloat(total.toFixed(2));
        }
    }
}